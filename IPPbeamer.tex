%% This is file `IPPbeamer.tex' version 2.2 (2022/07/23),
%% it is part of the IPP-bundle
%% Beamer-slides and tcolorbox-posters for IPP
%% ----------------------------------------------------------------------------
%%
%%  Copyright (C) 2020–2023 by Marei Peischl <marei@peitex.de>
%%
%% ============================================================================
%% This work may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either version 1.3c
%% of this license or (at your option) any later version.
%% The latest version of this license is in
%% http://www.latex-project.org/lppl.txt
%% and version 1.3c or later is part of all distributions of LaTeX
%% version 2008/05/04 or later.
%%
%% This work has the LPPL maintenance status `maintained'.
%%
%% Thecurrent maintainer of this work is
%%   Marei Peischl <marei@peitex.de>
%%
%% ============================================================================
%%
\documentclass[
	english,% main language as global option
	logo=false,% logo to be used in the headline. Pre-defined values are: asdex, w7x, false. Initial value is empty
	eurofusion=true, %initial value is false
	titlegraphic=false, %initial value is false
	]{ippbeamer}

\usepackage{babel} % language option has been set as global option

% some adjustments which are generally not available
\let\code\texttt
\let\file\texttt

% extended graphicspath to use separate image directories without the bundle being installed
\graphicspath{{}{img/}{img/titlegraphic/}}

\title{The IPPbeamer class}
\subtitle{presentations using \LaTeX}
\author{Marei Peischl\inst{1} \and Autor 2\inst{1,2}}
\institute{\inst{1}pei\TeX{} \and\inst{2}Institut 2}
\date{\today}
% Additional Logo to be placed in the footer
\logo{\includegraphics[height=\height]{example-image}}

\begin{document}


\frame{\titlepage}

\section{Introduction}

\begin{frame}{The IPPbeamer class}
	The IPPbeamer template is based on a documentclass called IPPbeamer.
	It adds some mechanisms for option parsing but besides that only loads the beamer class and the corresponding beamer theme, called IPP.
	
	The template consists of the following files:
	\begin{itemize}
		\item This document as well as its source code. (\file{IPPbeamer.tex})
	\end{itemize}
\end{frame}

\section{General setup}
\begin{frame}{Global and local options}
	IPPbeamer supports the change of layout options inside the document. Therefore the IPPbeamer specific options can be set using the \code{\textbackslash{}IPPbeamerSetup} macro. 
	
	See the demo document using options in the documentclass macro directly or the titlepage examples for changes inside the document.
\end{frame}

\begin{frame}[fragile]{Font selection}
	The PowerPoint theme uses Arial as main Font. As the Design Guideline prefers Roboto this template set the default to Roboto. Using Arial (if installed) or TeX Gyre Heros, which is the corresponding OpenSource equivalent, which is part of all common TeX distributions, is also possible. Use the \verb+arial+ option to switch.
\end{frame}

\begin{frame}{Fontsize configuration}
	The ippbeamer class supports the beamer options (\code{<xx>pt} or \code{smaller}/\code{larger}).
	
	\medskip
	The default fontsize of the theme is set to 11pt on a slide with height being 9\,cm. 
	
	\medskip
	This is larger than the default font size of the PowerPoint template. To match the PowerPoint variant, use the \code{smaller} option.
\end{frame}

\IPPbeamerSetup{headlinelogos={example-image,asdex,example-image}}
\begin{frame}[fragile]{Specific logo setup}
	The IPPbeamer class offers the possibility to add a custom Logo in the headline, which will also be shown on the titlepage.
	
	The option to adjust this is called \code{headlinelogos} and accepts the predefined values \code{asdex} and \code{w7x}. The logos are part of this template. If you require any other logo to be used there, feel free to pass a custom file name or path to the \code{headlinelogos} option.

	ippbeamer also offers the possibility of adding multiple logos to the headline. This can be done using the \verb+headlinelogos={list of logos}+ option. Since this might lead to issues with option parsing, in this case one should not use the documentclass options, but \verb+\IPPbeamerSetup+. This allows to set a list of allowed values for the logo option. They will be placed left to right.
\end{frame}
%undo the change to asdex logo
\IPPbeamerSetup{headlinelogos=}


\section{Frame examples}

\begin{frame}{Simple frame with title}
Content.

Remember that this theme does not support framesubtitles!
\end{frame}

\begin{frame}{Mechanism to handle too long headlines. This is an example of a very long headline including a linebreak.}
	While the use of overly long headlines is generally discouraged, ippbeamer allows headlines over two lines. The fontsize is decreased to fit into the frametitle block.
\end{frame}


\begin{frame}{Text beside image}
\begin{columns}[
	onlytextwidth,%otherwise the whole pagewidth would be used
	c, %align vertically centered
	]
\begin{column}{.5\linewidth}
	This is an example to use text beside an image. The columns environment is the easiest way to receive this setup.
	
	The space between the columns will be filled automatically. Be carefull to not use 100\,\% of the the linewidth.
\end{column}
\begin{column}{.45\linewidth}
	\includegraphics[width=\linewidth]
	   {example-image}
\end{column}
\end{columns}
\end{frame}

\begin{frame}[c]{Lists}
	The list mechanisms are exactly the same as for standard \LaTeX.
	\begin{columns}[
		T, %align vertically on the top, t would be top baseline,
		onlytextwidth
		]
		\column{0.3\textwidth}
		Items
		\begin{itemize}
			\item First \item Second \item Third
		\end{itemize}
		
		\column{0.3\textwidth}
		Enumerations
		\begin{enumerate}
			\item First \item Second \item Third
		\end{enumerate}
		
		\column{0.3\textwidth}
		Descriptions
		\begin{description}
		\item[first] First \item[second] Second \item[third] Third
		\end{description}
	\end{columns}
\end{frame}

\begin{frame}{Blocks}
	\begin{block}{Simple block with title}
		content
	\end{block}
	\begin{block}{}
		block without title
	\end{block}
\end{frame}

\begin{frame}{Block types}
	\begin{exampleblock}{Exampleblock}
		content
	\end{exampleblock}
	\begin{alertblock}{Alertblock}
		content
	\end{alertblock}
	\begin{example}[example environment]
		content
	\end{example}
\end{frame}

\begin{frame}[fragile]{Alerted text}
The beamer method to emphasize text is the alert mode.

By default this theme uses 50\,\% black for the alerted text. It can be changed. Mostly the colors IPP-blue or some dark red (IPP-red) are used.

You can change this by using:

\begin{verbatim}
\setbeamercolor*{alerted text}{
  bg=white,
  fg=<color for the desired text>
}
\end{verbatim}

Color examples:
\def\colorexample#1{\textcolor{#1}{\detokenize{#1}}}

\colorexample{MPG_green}
\colorexample{MPG_green_dark}
\colorexample{MPG_grey_dark}
\colorexample{MPG_grey}
\colorexample{MPG_grey_light}

\colorexample{MPG_green_light}
\colorexample{MPG_blue_dark}
\colorexample{MPG_blue_light}
\colorexample{MPG_orange}
	
\end{frame}

\section{Title material}

\begin{frame}[fragile]{Basic title setup}

Example for a basic title setup:
\begin{verbatim}
\title{The IPPbeamer class}
\subtitle{IPP Corporate Design using \LaTeX}
\author{Marei Peischl (pei\TeX{})}
\date{\today}
\frame{\titlepage}
\end{verbatim}
\end{frame}


\section{Examples for title setups}

\begin{frame}[fragile]{Titlepage modes}
	There are 3 options to change the titlepage:
	\begin{itemize}
	\item \code{logo=} see the headline setup. This one is just also visible on the titlepage
	\item \code{titlelogos=} adds multiple logos on the titlepage below the IPP logo. Overwrites the \code{logo=} setting.
	\item \code{titlegraphic=true/false/imagepath} Image below the title data. For the logo modes \code{asdex} and \code{w7x} there are predefined ones.
	
	If \code{titlegraphic=true} the predefined image will be selected. This might lead to an error message, if you use a combination for which no predefined image has been added. For example this is the case for the combination of \code{logo=asdex} and \code{titlegraphic=true}.
	
	You can also set a titlegraphic and its scaling manually, using the corresponding macro \code{\textbackslash{}titlegraphic{<content>}}.
	\item \code{eurofusion=true/false} this option is a switch to enable the acknowledgement on the titlepage. Its initialized to false.
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Customize the EUROfusion information}
	The text next to the EU flag is saved as macro (\verb+\EUROfusionInfo+) which is initialized as:
	\begin{verbatim}
	\newcommand*{\EUROfusionInfo}{
	This work has been carried out within the framework
	of the EUROfusion Consortium, funded by the European
	Union via the Euratom Research and Training Programme
	(Grant Agreement No 101052200 — EUROfusion). Views
	and opinions expressed are however those of the
	author(s) only and do not necessarily reflect those
	of the European Union or the European Commission.
	Neither the European Union nor the European
	Commission can be held responsible for them.
	}
	\end{verbatim}
	Use \verb+\renewcommand*+ to redefine it.

\end{frame}

\IPPbeamerSetup{titlelogos=asdex, eurofusion=false, titlegraphic=false}
\subtitle{titlelogos=asdex, eurofusion=false, titlegraphic=false}
\frame{\titlepage}

\IPPbeamerSetup{titlelogos={asdex, example-image}, eurofusion=true, titlegraphic=false}
\subtitle{titlelogos={asdex, example-image}, eurofusion=true, titlegraphic=false}
\frame{\titlepage}

\IPPbeamerSetup{titlelogos=asdex, eurofusion=true, titlegraphic=true}
\subtitle{titlelogos=asdex, eurofusion=true, titlegraphic=true}
\frame{\titlepage}

\IPPbeamerSetup{titlelogos=asdex, eurofusion=false, titlegraphic=true}
\subtitle{titlelogos=asdex, eurofusion=false, titlegraphic=true}
\frame{\titlepage}

\IPPbeamerSetup{titlelogos=,titlegraphic=example-image}
\subtitle{titlelogos=asdex, eurofusion=false, titlegraphic=example-image (autoscaled and clipped)}
\frame{\titlepage}

\begin{frame}[fragile]{Additional information on eps images}
	eps images are converted to pdf before being included. All common TeX distributions therefore include the epstopdf package. This is automatically loaded for TeX Live but has to be loaded manually in MikTeX.

	\begin{verbatim}
	\usepackage{epstopdf}
	\end{verbatim}

	Please ensure to load it if you want to share the file across different or unknown distributions.
	Afterwards the graphicx package is automatically able to convert these files on the fly. E.\,g. if the file is names epstest.eps the following will load it as usual:

	\begin{verbatim}
	\includegraphics{epstest}
	\end{verbatim}
\end{frame}

\end{document}

