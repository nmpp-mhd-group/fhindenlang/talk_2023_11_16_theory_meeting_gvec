[![pipeline status](https://gitlab.mpcdf.mpg.de/nmpp-mhd-group/fhindenlang/talk_2023_11_16_theory_meeting_gvec/badges/main/pipeline.svg)](https://gitlab.mpcdf.mpg.de/nmpp-mhd-group/fhindenlang/talk_2023_11_16_theory_meeting_gvec/-/commits/main)

# Title 
### Beyond cylinder coordinates: Compute MHD equilibria with a generalized Frenet-frame in GVEC
### [ ==> Download pdf <== ](https://gitlab.mpcdf.mpg.de/nmpp-mhd-group/fhindenlang/talk_2023_11_16_theory_meeting_gvec/-/jobs/artifacts/main/raw/Hindenlang_GVEC_theory_meeting_2023_11_16.pdf?job=pdflatex_final)

# Authors
### Florian Hindenlang , Gabriel Plunk

# Date and Location
### 16 November 2023
### IPP theory meeting 2023 in Harnackhaus,Berlin

# Acknowledgements
Thanks to: 

Carolin Nuehrenberg , Omar Maj , Tiago Ribeiro , Robert Köberl , Alan Goodman ,
Katia Camacho , Matt Landreman 

The `beamer` template was taken from
 the “IPP Bundle” version 2.6 (2023/03/21) [gitlab.mpcdf.mpg.de/g-peiTeX/ipp-latex.git](https://gitlab.mpcdf.mpg.de/g-peiTeX/ipp-latex.git) 

